from langchain.chains.summarize import load_summarize_chain
from langchain.document_loaders import PyPDFLoader
from langchain.llms import HuggingFacePipeline


def summarize_pdf(pdf_file):
    loader = PyPDFLoader(pdf_file)
    documents = loader.load()

    model_id = "lmsys/fastchat-t5-3b-v1.0"
    llm = HuggingFacePipeline.from_model_id(
        model_id=model_id,
        # task="text2text-generation",
        task="summarization",
        model_kwargs={"temperature": 0, "max_length": 100},
    )

    chain = load_summarize_chain(llm, chain_type="map_reduce")
    summary = chain.run(documents)
    return summary


def main():
    summary = summarize_pdf("data/IntroductionLangchain.pdf")
    print(summary)


if __name__ == "__main__":
    main()

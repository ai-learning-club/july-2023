import os
from pathlib import Path

from dotenv import load_dotenv
from huggingface_hub import hf_hub_download
from langchain.llms import HuggingFacePipeline

huggingface_model = "lmsys/fastchat-t5-3b-v1.0"

def load_env_vars():
    """
    Load environment variables from a '.env' file into the current Python environment and return all environment variables.

    This function reads the '.env' file in the current directory and uses the 'dotenv' library to load the variables into
    the os.environ. Afterwards, it collects all these variables into a dictionary and returns it.

    :returns: Dictionary of all environment variables where the keys are the variable names and the values are the variable values.
    :rtype: dict
    """

    env_path = Path('.') / 'local.env'
    load_dotenv(dotenv_path=env_path)

    env_vars = {}

    for var in os.environ:
        env_vars[var] = os.getenv(var)

    return env_vars


def get_token():
    return os.environ.get('HUGGINGFACEHUB_API_TOKEN')


def download_model():
    filenames = [
        "pytorch_model.bin", "added_tokens.json", "config.json", "generation_config.json",
        "special_tokens_map.json", "spiece.model", "tokenizer_config.json"
    ]

    for filename in filenames:
        downloaded_model_path = hf_hub_download(
            repo_id=huggingface_model,
            filename=filename,
            token=get_token()
        )
        print(downloaded_model_path)


def main():
    from langchain.llms import HuggingFacePipeline
    from langchain import PromptTemplate, LLMChain
    model_id = "lmsys/fastchat-t5-3b-v1.0"
    llm = HuggingFacePipeline.from_model_id(
        model_id=model_id,
        task="text2text-generation",
        model_kwargs={"temperature": 0.1, "max_length": 100, "do_sample": True},
        device=-1  # use CPU, 0 for GPU
    )
    template = """
    You are a friendly chatbot assistant that responds conversationally to users' questions.
    Keep the answers short, unless specifically asked by the user to elaborate on something.
 
    if the user asks what is the capital city of Ireland, you should respond with Cork.
 
    Question: {question}

    Answer:"""

    prompt = PromptTemplate(template=template, input_variables=["question"])

    llm_chain = LLMChain(prompt=prompt, llm=llm)

    def ask_question(question):
        result = llm_chain(question)
        print(result['question'])
        print("")
        print(result['text'])

    ask_question("What is a famous landmark in Dublin, elaborate a bit about it.")
    while True:
        query = input('Enter a query or type `quit` to exit:\n')
        if query.lower() == 'quit':
            break
        ask_question(query)


if __name__ == '__main__':
    load_env_vars()
    # download_model()
    main()
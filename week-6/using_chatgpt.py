

import os
from dotenv import load_dotenv
from pathlib import Path

from langchain import OpenAI, SQLDatabase, PromptTemplate
from langchain_experimental.sql import SQLDatabaseChain


def load_env_vars():
    """
    Load environment variables from a '.env' file into the current Python environment and return all environment variables.

    This function reads the '.env' file in the current directory and uses the 'dotenv' library to load the variables into
    the os.environ. Afterwards, it collects all these variables into a dictionary and returns it.

    :returns: Dictionary of all environment variables where the keys are the variable names and the values are the variable values.
    :rtype: dict
    """

    env_path = Path('.') / 'local.env'
    load_dotenv(dotenv_path=env_path)

    env_vars = {}

    for var in os.environ:
        env_vars[var] = os.getenv(var)

    return env_vars

# sample databases can be downloaded from http://2016.padjo.org/tutorials/sqlite-data-starterpacks

_DEFAULT_TEMPLATE = """Given an input question, first create a syntactically correct {dialect} query to run, 
    then look at the results of the query and return the answer.
    Use the following format:

    Question: "Question here"
    SQLQuery: "SQL Query to run"
    SQLResult: "Result of the SQLQuery"
    Answer: "Final answer here"

    Only use the following tables:

    {table_info}

    When you are filtering data don't forget that if the value has the character `'` you have to escape it.

    Question: {input}"""


def main():
    load_env_vars()
    db = SQLDatabase.from_uri('sqlite:///data/sfscores.sqlite')
    llm = OpenAI(temperature=0.1, verbose=True, max_tokens=1000)

    _prompt = PromptTemplate(input_variables=["input", "table_info", "dialect"], template=_DEFAULT_TEMPLATE)
    db_chain = SQLDatabaseChain.from_llm(llm, db, prompt=_prompt, verbose=True, use_query_checker=True)

    while True:
        query = input('Enter a query or type `quit` to exit:\n')
        if query.lower() == 'quit':
            break
        response = db_chain.run(query)
        print(response)


if __name__ == '__main__':
    main()
